// Copyright 2018 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.google.firebase.samples.apps.mlkit.facedetection;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.support.annotation.NonNull;

import com.google.android.gms.vision.CameraSource;
import com.google.firebase.ml.vision.common.FirebaseVisionPoint;
import com.google.firebase.ml.vision.face.FirebaseVisionFace;
import com.google.firebase.ml.vision.face.FirebaseVisionFaceLandmark;
import com.google.firebase.samples.apps.mlkit.GraphicOverlay;
import com.google.firebase.samples.apps.mlkit.GraphicOverlay.Graphic;
import com.google.firebase.samples.apps.mlkit.R;

/**
 * Graphic instance for rendering face position, orientation, and landmarks within an associated
 * graphic overlay view.
 */
public class FaceGraphic extends Graphic {
    private static final float FACE_POSITION_RADIUS = 10.0f;
    private static final float ID_TEXT_SIZE = 40.0f;
    private static final float ID_Y_OFFSET = 50.0f;
    private static final float ID_X_OFFSET = -50.0f;
    private static final float BOX_STROKE_WIDTH = 5.0f;

    private static final int[] COLOR_CHOICES = {
            Color.BLUE, Color.CYAN, Color.GREEN, Color.MAGENTA, Color.RED, Color.WHITE, Color.YELLOW
    };
    private static int currentColorIndex = 0;

    private int facing;

    private final Paint facePositionPaint;
    private final Paint idPaint;
    private final Paint boxPaint;

    private volatile FirebaseVisionFace firebaseVisionFace;

    public FaceGraphic(GraphicOverlay overlay) {
        super(overlay);

//        currentColorIndex = (currentColorIndex + 1) % COLOR_CHOICES.length;
        currentColorIndex = 2;
        final int selectedColor = COLOR_CHOICES[currentColorIndex];

        facePositionPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        facePositionPaint.setColor(selectedColor);

        idPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        idPaint.setColor(selectedColor);
        idPaint.setTextSize(ID_TEXT_SIZE);

        boxPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        boxPaint.setColor(selectedColor);
        boxPaint.setStyle(Paint.Style.STROKE);
        boxPaint.setStrokeWidth(BOX_STROKE_WIDTH);
    }

    /**
     * Updates the face instance from the detection of the most recent frame. Invalidates the relevant
     * portions of the overlay to trigger a redraw.
     */
    public void updateFace(FirebaseVisionFace face, int facing) {
        firebaseVisionFace = face;
        this.facing = facing;
        postInvalidate();
    }



    public static void setDrawable(Drawable drawable) {
        d = drawable;
    }

    /**
     * Draws the face annotations for position on the supplied canvas.
     */
    @Override
    public void draw(Canvas canvas) {
        FirebaseVisionFace face = firebaseVisionFace;
        if (face == null) {
            return;
        }
//        if (d == null) {
//            d = getApplicationContext().getResources().getDrawable(R.drawable.ic_sunglass_star);
//        }

        // Draws a circle at the position of the detected face, with the face's track id below.
        float x = translateX(face.getBoundingBox().centerX());
        float y = translateY(face.getBoundingBox().centerY());
        if (testMode) {
            canvas.drawCircle(x, y, FACE_POSITION_RADIUS, facePositionPaint);
            canvas.drawText("id: " + face.getTrackingId(), x + ID_X_OFFSET, y + ID_Y_OFFSET, idPaint);
            canvas.drawText(
                    "happiness: " + String.format("%.2f", face.getSmilingProbability()),
                    x + ID_X_OFFSET * 3,
                    y - ID_Y_OFFSET,
                    idPaint);
            canvas.drawText(
                    "HeadEulerAngleZ: " + String.format("%.2f", face.getHeadEulerAngleZ()),
                    x + ID_X_OFFSET * 3,
                    y - 90,
                    idPaint);
            canvas.drawText(
                    "HeadEulerAngleY: " + String.format("%.2f", face.getHeadEulerAngleY()),
                    x + ID_X_OFFSET * 3,
                    y - 120,
                    idPaint);

            if (facing == CameraSource.CAMERA_FACING_FRONT) {
                canvas.drawText(
                        "right eye: " + String.format("%.2f", face.getRightEyeOpenProbability()),
                        x - ID_X_OFFSET,
                        y,
                        idPaint);
                canvas.drawText(
                        "left eye: " + String.format("%.2f", face.getLeftEyeOpenProbability()),
                        x + ID_X_OFFSET * 6,
                        y,
                        idPaint);
            } else {
                canvas.drawText(
                        "left eye: " + String.format("%.2f", face.getLeftEyeOpenProbability()),
                        x - ID_X_OFFSET,
                        y,
                        idPaint);
                canvas.drawText(
                        "right eye: " + String.format("%.2f", face.getRightEyeOpenProbability()),
                        x + ID_X_OFFSET * 6,
                        y,
                        idPaint);
            }
        }
        // Draws a bounding box around the face.
        float xOffset = scaleX(face.getBoundingBox().width() / 2.0f);
        float yOffset = scaleY(face.getBoundingBox().height() / 2.0f);
        float left = x - xOffset;
        float top = y - yOffset;
        float right = x + xOffset;
        float bottom = y + yOffset;
        if (testMode) {
            canvas.drawRect(left, top, right, bottom, boxPaint);

            // draw landmarks
//        drawLandmarkPosition(canvas, face, FirebaseVisionFaceLandmark.BOTTOM_MOUTH);
//        drawLandmarkPosition(canvas, face, FirebaseVisionFaceLandmark.LEFT_CHEEK);
//        drawLandmarkPosition(canvas, face, FirebaseVisionFaceLandmark.LEFT_EAR);
//        drawLandmarkPosition(canvas, face, FirebaseVisionFaceLandmark.LEFT_MOUTH);
            drawLandmarkPosition(canvas, face, FirebaseVisionFaceLandmark.LEFT_EYE);
//        drawLandmarkPosition(canvas, face, FirebaseVisionFaceLandmark.NOSE_BASE);
//        drawLandmarkPosition(canvas, face, FirebaseVisionFaceLandmark.RIGHT_CHEEK);
//        drawLandmarkPosition(canvas, face, FirebaseVisionFaceLandmark.RIGHT_EAR);
            drawLandmarkPosition(canvas, face, FirebaseVisionFaceLandmark.RIGHT_EYE);
//        drawLandmarkPosition(canvas, face, FirebaseVisionFaceLandmark.RIGHT_MOUTH);
        }

        FirebaseVisionFaceLandmark LE = face.getLandmark(FirebaseVisionFaceLandmark.LEFT_EYE);
        FirebaseVisionFaceLandmark RE = face.getLandmark(FirebaseVisionFaceLandmark.RIGHT_EYE);
        int LeftEyeGlassX = 0;
        int RightEyeGlassX = 0;
        int TopEyeGlassY = 0;
        int BottmEyeGlassY = 0;

        if (RE != null && LE != null) {
            int leftEyeX = (int) translateX(LE.getPosition().getX());
            int leftEyeY = (int) translateY(LE.getPosition().getY());
            int rightEyeX = (int) translateX(RE.getPosition().getX());
            int rightEyeY = (int) translateY(RE.getPosition().getY());
            if (facing == CameraSource.CAMERA_FACING_FRONT) {
                LeftEyeGlassX = leftEyeX - (int) (xOffset / 2);
                RightEyeGlassX = rightEyeX + (int) (xOffset / 2);
                TopEyeGlassY = leftEyeY - (int) (xOffset / 3);
                BottmEyeGlassY = rightEyeY + (int) (xOffset / 3);
            } else {
                LeftEyeGlassX = rightEyeX - (int) (xOffset / 2);
                RightEyeGlassX = leftEyeX + (int) (xOffset / 2);
                TopEyeGlassY = rightEyeY - (int) (xOffset / 3);
                BottmEyeGlassY = leftEyeY + (int) (xOffset / 3);
            }
            int glassWidth = (int) (xOffset * 1.8);
            if (d != null)
                d.setBounds(LeftEyeGlassX, TopEyeGlassY,
                        LeftEyeGlassX + glassWidth, TopEyeGlassY + glassWidth);
        }
        if (facing == CameraSource.CAMERA_FACING_FRONT) {
            if (d != null)
                rotatedD = getRotateDrawable(d, face.getHeadEulerAngleZ(), x, y);
        } else {
            if (d != null)
                rotatedD = getRotateDrawable(d, -face.getHeadEulerAngleZ(), x, y);
        }
//        if (LeftEyeGlassX != 0 || BottmEyeGlassY != 0) {
            if (rotatedD != null) {
                rotatedD.draw(canvas);
            } else if (d != null) {
                d.draw(canvas);
            }
//        }

    }

    float AngleZ = 0;
    private static Drawable d, rotatedD;
    public static boolean testMode = true;
//    Bitmap getbitmap(int glassWidth) {
//        try {
//            Bitmap bitmap;
//            bitmap = Bitmap.createBitmap(glassWidth, glassWidth, Bitmap.Config.ARGB_8888);
//            Canvas canvas = new Canvas(bitmap);
//            d.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
//            d.draw(canvas);
//            return bitmap;
//        } catch (OutOfMemoryError e) {
//            // Handle the error
//            return null;
//        }
//    }
//
//    public static Bitmap RotateBitmap(Bitmap source, float angle) {
//        if (source == null) return null;
//        Matrix matrix = new Matrix();
//        matrix.postRotate(angle);
//        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
//    }

    private Drawable getRotateDrawable(final Drawable d, final float angle, final float x,
                                       final float y) {
        final Drawable[] arD = {d};
        return new LayerDrawable(arD) {
            @Override
            public void draw(final Canvas canvas) {
                canvas.save();
                canvas.rotate(angle, x, y);
                super.draw(canvas);
                canvas.restore();
            }
        };
    }

    private void drawLandmarkPosition(Canvas canvas, FirebaseVisionFace face, int landmarkID) {
        FirebaseVisionFaceLandmark landmark = face.getLandmark(landmarkID);
        if (landmark != null) {
            FirebaseVisionPoint point = landmark.getPosition();
            canvas.drawCircle(
                    translateX(point.getX()),
                    translateY(point.getY()),
                    10f, idPaint);
        }
    }
}
